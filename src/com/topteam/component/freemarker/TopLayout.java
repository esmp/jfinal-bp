package com.topteam.component.freemarker;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.topteam.frame.controllor.BaseController;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class TopLayout extends TopTemplateModel{
	
	public final static String TAG ="layout";
	
	private BaseController controller;
	
	private String id;
	
	private String style; 
	
	private boolean fit;
	
	private int width;
	
	private int height;
	
	public TopLayout(BaseController controller){
		this.controller = controller;
	}
	
	private void initPara(Map params, Environment env) throws TemplateException {
		id = getStrPara("id", params, env);
		style = getStrPara("style", params, env);
		fit = getBoolPara("fit", params, env, fit);
		width = getIntPara("width", params, env);
		height = getIntPara("height", params, env);
	}

	@Override
	public void execute(Environment env, Map para, TemplateModel[] arg2,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		initPara(para, env);
		Writer w = env.getOut();
		w.write("<div class=\"easyui-layout\"  ");
		if(fit){
			w.write("data-option=\"fit:true\"");
		}else{
			w.write(" style=\"width:"+width+"px;height:"+height+"px;\"");
		}
		w.write(">");
		body.render(w);
		w.write("</div>");
	}

}
