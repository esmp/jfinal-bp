package com.topteam.utility;

public class StringUtil {

	public static boolean isNull(String s){
		return s==null||s.equals("");
	}
	
	public static boolean isNotNull(String s){
		return s!=null&&!s.equals("");
	}
	
	public static String nextCode(String code){
		Long num = Long.valueOf("1"+code)+1;
		return String.valueOf(num).substring(1, code.length()+1);
	}
	
	public static void main(String[] args) {
		System.out.println(nextCode("2330"));
	}
}
