package com.topteam.frame.controllor;

import com.jfinal.core.Controller;
import com.topteam.security.UserSession;

public class BaseController extends Controller{

	@Override
	public void initAttr() {
		setAttr("root", getRequest().getContextPath());
		UserSession userSession = getUserSession();
		if(userSession!=null){
			setAttr("userTheme",getUserSession().getUserTheme());
			setAttr("sysMsgPosition",getUserSession().getSysMsgPosition());
			setAttr("tableSize",getUserSession().getTableSize());
		}
	}
	
	public UserSession getUserSession(){
		return this.getSessionAttr("userSession");
	}
	
}
