package com.topteam.frame.vo;

import java.util.Date;

public class FrameMailVO {

	private String id;
	
	private String title;
	
	private String sendUser;
	
	private Date sendTime;
	
	private boolean hasCheck;
	
	private boolean important;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSendUser() {
		return sendUser;
	}

	public void setSendUser(String sendUser) {
		this.sendUser = sendUser;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public boolean isHasCheck() {
		return hasCheck;
	}

	public void setHasCheck(boolean hasCheck) {
		this.hasCheck = hasCheck;
	}

	public boolean isImportant() {
		return important;
	}

	public void setImportant(boolean important) {
		this.important = important;
	}
	
	
}
