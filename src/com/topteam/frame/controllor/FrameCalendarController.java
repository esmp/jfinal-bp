package com.topteam.frame.controllor;

import java.util.Calendar;

import com.topteam.core.Path;
import com.topteam.frame.vo.FrameCalendarVO;


@Path("/framecalendar")
public class FrameCalendarController extends BaseController{

	public void add(){
		renderJson();
	}
	
	public void viewCal(){
		FrameCalendarVO calendarVO = new FrameCalendarVO();
		calendarVO.setSubject("测试");
		Calendar c = Calendar.getInstance();
		calendarVO.setStartTime(c.getTime());
		c.set(Calendar.HOUR_OF_DAY, 20);
		calendarVO.setEndTime(c.getTime());
		renderJson("{'start':'2013-5-13 07:00','end':'2013-5-13 11:30','events':[[1,'测试','2013-5-13 07:00','2013-5-13 11:30',1,0,1,'会议','/www','无锡']]}"); 
	}
}
