package com.topteam.frame.controllor;

import com.jfinal.config.Routes;

public class FrameRoutes extends Routes {

	@Override
	public void config() {
		// ------- 安全登录
		this.add(SecurityController.class);
		
		this.add(ErrorController.class);
		
		// 附件上传
		this.add(UploadController.class);

		this.add(FrameMenuController.class);

		this.add(FrameDeptController.class);
		this.add(FrameUserController.class);
		this.add(FrameRoleTypeController.class);
		this.add(FrameRoleController.class);
		this.add(FrameCalendarController.class);
		this.add(FrameMailController.class);
		this.add(FrameUserConfigController.class);
		this.add(FrameDeptDutyController.class);
		this.add(UserRoleRelationController.class);
		this.add(FrameAuthorizeController.class);
		this.add(FrameCmsController.class);
		
	}

}
