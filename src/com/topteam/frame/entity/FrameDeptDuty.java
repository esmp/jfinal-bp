package com.topteam.frame.entity;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.ModelBean;

@ModelBean("FrameDeptDuty")
public class FrameDeptDuty extends Model<FrameDeptDuty>{

	public final static FrameDeptDuty dao = new FrameDeptDuty();
}
