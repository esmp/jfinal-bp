package com.topteam.plugin;

import com.jfinal.plugin.IPlugin;

public class AutoScanControllorPlugin implements IPlugin{
	@Override
	public boolean start() {
		System.out.println("AutoScanControllorPlugin ---- start");
		return true;
	}

	@Override
	public boolean stop() {
		System.out.println("AutoScanControllorPlugin ---- end");
		return true;
	}

}
