package com.topteam.frame.entity;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.ModelBean;

@ModelBean("FramePortal")
public class FramePortal extends Model<FramePortal>{

	public final static FramePortal dao = new FramePortal();
}
