package com.topteam.frame.controllor;

import java.util.UUID;

import com.jfinal.plugin.activerecord.Page;
import com.topteam.component.easyui.Paginate;
import com.topteam.core.Path;
import com.topteam.frame.entity.FrameRoleType;

@Path("/frameroletype")
public class FrameRoleTypeController extends BaseController {

	public void list() {
		render("/frame/FrameRoleTypeList.html");
	}

	public void table() {
		int page = getParaToInt("page");
		int size = getParaToInt("rows");
		Page<FrameRoleType> list = FrameRoleType.dao.getAllRoleType(page, size);
		Paginate paginate = Paginate.build(list);
		renderJson(paginate);
	}

	public void add() {
		FrameRoleType m = getModel(FrameRoleType.class, "FrameRoleType");
		m.set("id", UUID.randomUUID().toString());
		m.save();
		renderText("保存成功", "text/html");
	}
	
	public void update() {
		FrameRoleType m = getModel(FrameRoleType.class, "FrameRoleType");
		m.update();
		renderText("修改成功","text/html");
	}
	
	public void edit(){
		FrameRoleType d = FrameRoleType.dao.findById(getPara());
		setAttr("FrameRoleType", d);
		renderFreeMarker("/frame/FrameRoleTypeEditPage.html");
	}
	
	public void delete(){
		String ids = getPara("ids");
		FrameRoleType.dao.deleteIds(ids);
		renderJson();
	}
}
