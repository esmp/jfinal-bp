package com.topteam.frame.controllor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.topteam.component.easyui.EasyDataTableModel;
import com.topteam.component.easyui.EasyTreeModel;
import com.topteam.component.easyui.EasyTreeNode;
import com.topteam.component.easyui.Paginate;
import com.topteam.core.Path;
import com.topteam.frame.entity.FrameMenu;

@Path("/framemenu")
public class FrameMenuController extends BaseController{

	public void list(){
		render("/frame/FrameMenuList.html");
	}
	
	public void save(){
		FrameMenu m = getModel(FrameMenu.class,"menu");
		m.set("id", UUID.randomUUID().toString());
		m.set("menuCode", FrameMenu.dao.getNextCode(m.getStr("parentId")));
		m.save();
		renderText("保存成功","text/html");
	}
	
	public void update(){
		FrameMenu m = getModel(FrameMenu.class,"menu");
		String oldParentId = getPara("parentId");
		
		// 判断父节点是否改变，是否需要重新生成code
		boolean flag = false;
		if(oldParentId == null){
			if(m.getStr("parentId")!=null)
				flag = true;
		}else{
			if(m.getStr("parentId")==null || !oldParentId.equals(m.getStr("parentId")))
				flag = true;
		}
		if(flag){
			String newCode = FrameMenu.dao.getNextCode(m.getStr("parentId"));
			FrameMenu.dao.updateSubCode(m,newCode);
			m.set("menuCode", newCode);
		}else{
			m.set("menuCode", m.getStr("menuCode"));
			
		}
		m.update();
		renderText("修改成功","text/html");
	}
	
	public void edit(){
		FrameMenu m = FrameMenu.dao.findById(getPara());
		setAttr("menu", m);
		renderFreeMarker("/frame/FrameMenuEditPage.html");
	}
	
	public void add(){
		setAttr("parentId", getPara("parentId"));
		setAttr("menu", new FrameMenu());
		renderFreeMarker("/frame/FrameMenuAddPage.html");
	}
	
	public void delete(){
		String ids = getPara("ids");
		FrameMenu.dao.deleteIds(ids);
		renderText("删除成功","text/html");
	}
	
	public void table(){
		EasyDataTableModel model = new EasyDataTableModel(this) {
			@Override
			public Page<FrameMenu> fechData(int page, int size) {
				String code = getPara("code");
				Page<FrameMenu> p = null;
				if(code==null){
					p= FrameMenu.dao.getMenuPage(page, size);
				}else{
					p = FrameMenu.dao.getMenusByCode(code,page, size);
				}
				return p;
			}
		};
		renderJson(model.toJson());
	}
	
	public void leftTree(){
		EasyTreeModel<FrameMenu> model = new EasyTreeModel<FrameMenu>(this) {
			private static final long serialVersionUID = -5310992744165040365L;
			
			@Override
			public EasyTreeNode model2Node(FrameMenu t) {
				EasyTreeNode treeNode = new EasyTreeNode();
				treeNode.setId(t.getStr("id"));
				treeNode.setText(t.getStr("menuName"));
				Map<String, String> map = new HashMap<String, String>();
				map.put("menuCode", t.getStr("menuCode"));
				treeNode.setAttributes(map);
				treeNode.setHasChild(t.getLong("has")>0L);
				return treeNode;
			}
			
			@Override
			public List<FrameMenu> fechDate(String id) {
				List<FrameMenu> list = new ArrayList<FrameMenu>();
				if(id == null){
					String sql = "select t.*, case when (SELECT count(1) from FrameMenu t2 where t2.parentId=t.id)>0 then 1 ELSE 0 END as has  from FrameMenu t where t.parentId is null order by t.order desc";
					list = FrameMenu.dao.find(sql);
				}
				else{
					String sql = "select t.*, case when (SELECT count(1) from FrameMenu t2 where t2.parentId=t.id)>0 then 1 ELSE 0 END as has  from FrameMenu t where t.parentId=? order by t.order desc";
					list = FrameMenu.dao.find(sql,id);
				}
				return list;
			}
		};
		
		renderJson(model.toJson());
	}
}
