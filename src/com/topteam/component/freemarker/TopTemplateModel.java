package com.topteam.component.freemarker;

import java.io.Serializable;
import java.util.Map;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;

public abstract class TopTemplateModel implements TemplateDirectiveModel,Serializable{

	public Boolean getBoolPara(String key, Map params,Environment env, boolean def) throws TemplateException{
		if(!params.containsKey(key)){
			return def;
		}else{
			Boolean f = Boolean.valueOf(false);
			try{
				f = Boolean.valueOf(params.get(key).toString());
			}catch(Exception e){
				throw new TemplateException("获取Boolean型参数错误", env);
			}
			return f;
		}
	}
	
	public String getStrPara(String key, Map params,Environment env) throws TemplateException{
		if(!params.containsKey(key)){
			return null;
		}else{
			return params.get(key).toString();
		}
	}
	
	public int getIntPara(String key, Map params,Environment env) throws TemplateException{
		if(!params.containsKey(key)){
			return -1;
		}else{
			return Integer.valueOf(params.get(key).toString());
		}
	}
	
	public Double getDoublePara(String key, Map params,Environment env) throws TemplateException{
		if(!params.containsKey(key)){
			return -1.0;
		}else{
			return Double.valueOf(params.get(key).toString());
		}
	}
}
