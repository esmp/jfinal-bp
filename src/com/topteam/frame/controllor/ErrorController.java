package com.topteam.frame.controllor;

import com.jfinal.core.ActionException;
import com.topteam.core.Path;

@Path("/error")
public class ErrorController extends BaseController{

	public void index(){
		int code = getParaToInt();
		render("/"+code+".html");
	}
	
	public void test() throws Exception{
		throw new Exception("www");
	}
}
