var FrameTab = {
	
};

FrameTab.addTab = function(title, href,icon) {
	var tt = $($("#frametab").length!=0?$("#frametab"):$('#frametab', parent.document));
	if(tt.tabs("exists",title)){
		tt.tabs('select',title);
		FrameTab.refreshTab({tabTitle:title,url:href});
	}else{
		var content;
		if (href&&href!='#'){  
            content = '<iframe scrolling="no" frameborder="0"  src="'+href+'" style="width:100%;height:100%;"></iframe>';  
        } else  {  
            content = '未实现';  
        }  
        var tab = tt.tabs('add',{  
            title:title,
            closable:true,  
            content:content,  
            iconCls:icon||'icon-default'  
        });
        $(tab).tabs('getSelected').css("overflow","hidden");
	}
};

FrameTab.refreshTab = function(cfg) {
	var refresh_tab = cfg.tabTitle ? $("#frametab").tabs('getTab', cfg.tabTitle)
			: FrameTab.tabs.tabs('getSelected');
	if (refresh_tab && refresh_tab.find('iframe').length > 0) {
		var _refresh_ifram = refresh_tab.find('iframe')[0];
		var refresh_url = cfg.url ? cfg.url : _refresh_ifram.src;
		_refresh_ifram.contentWindow.location.href = refresh_url;
	}
};

$(function(){
	$("#frametab").tabs({
		onSelect:function(){
			var iframe = $(this).find("iframe")[0];
			iframe.contentWindow.location.href = iframe.src;
		}
	});
});


var FrameMsg = {};

FrameMsg.show = function(msg, p,timeout){
	var to = timeout? timeout:10000;
	$.messager.show({  
        title:'系统消息提示',  
        msg:msg,
        timeout:to,
        showType:'slide',  
        style:FrameMsg.position(p)
    });
};

FrameMsg.position = function(position){
	var posi={};
	if(position == 'top-left'){
		posi = { right:'',  
            left:0,  
            top:document.body.scrollTop+document.documentElement.scrollTop,  
            bottom:''};
	}else if(position=='top-center'){
		posi = {right:'',  
                top:document.body.scrollTop+document.documentElement.scrollTop,  
                bottom:''};
	}else if(position == 'top-right'){
		posi = {left:'',  
                right:0,  
                top:document.body.scrollTop+document.documentElement.scrollTop,  
                bottom:''};
	}else if(position == 'center-left'){
		posi = {left:0,  
                right:'',  
                bottom:''};
	}else if(position == 'center'){
		posi = { right:'',  
                bottom:''};
	}else if(position == 'center-right'){
		posi = {left:'',  
                right:0,  
                bottom:''};
	}else if(position == 'bottom-left'){
		posi = {left:0,  
                right:'',  
                top:'',  
                bottom:-document.body.scrollTop-document.documentElement.scrollTop};
	}else if(position == 'bottom-center'){
		posi = {right:'',  
                top:'',  
                bottom:-document.body.scrollTop-document.documentElement.scrollTop};
	}else if(position == 'bottom-right'){
		posi = {title:'My Title',  
                msg:'The message content',  
                showType:'show'};
	}
	return posi;
};




