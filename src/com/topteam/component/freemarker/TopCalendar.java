package com.topteam.component.freemarker;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.topteam.frame.controllor.BaseController;
import com.topteam.utility.StringUtil;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class TopCalendar extends TopTemplateModel{
	
	private static final long serialVersionUID = 5972757003891799129L;

	public final static String TAG = "calendar";
	
	private String id;
	
	private String name;
	
	private String style;
	
	private String formatter;
	
	private boolean showTime;
	
	private String value;
	
	public TopCalendar(BaseController bc) {
	}

	private void initPara(Map params, Environment env) throws TemplateException {
		id = getStrPara("id", params, env);
		name = getStrPara("name", params, env);
		style = getStrPara("style", params, env);
		formatter = getStrPara("formatter", params, env);
		showTime = getBoolPara("showTime", params, env, showTime);
	}

	@Override
	public void execute(Environment env, Map param, TemplateModel[] arg2,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		initPara(param, env);
		Writer w = env.getOut();
		w.write("<input id=\""+id+"\" ");
		if(StringUtil.isNotNull(style))
			w.write("style=\""+style+"\"");
		if(StringUtil.isNotNull(name))
			w.write("name=\""+name+"\"");
		if(showTime){
			w.write(" class=\"easyui-datetimebox\" ");
		}else{
			w.write(" class=\"easyui-datebox\" ");
		}
		if(StringUtil.isNotNull(formatter))
			w.write(" data-options=\"formatter:function(date){DateUtil.date2String(date,'"+formatter+"');},parser:function(s){DateUtil.string2Date(s,'"+formatter+"');}\"");
		w.write("></input>");
	}

}
