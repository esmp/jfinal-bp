package com.topteam.component.easyui;

public class EasyAccordion {
	private String title;
	private String style;
	private String styleClass;
	private String id;
	private String easyOptions;
	
	private Object data;
	private boolean select;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getStyleClass() {
		return styleClass;
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEasyOptions() {
		return easyOptions;
	}

	public void setEasyOptions(String easyOptions) {
		this.easyOptions = easyOptions;
	}

	public boolean isSelect() {
		return select;
	}

	public void setSelect(boolean select) {
		this.select = select;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
