package com.topteam.utility;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.topteam.frame.vo.Porperty;

public class VOUtil {

	public static List<Map<String, String>> porperty2MapList(Object o) {
		Class<?> cls = o.getClass();
		Field[] fs = cls.getDeclaredFields();
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		for (Field f : fs) {
			Map<String, String> map = new HashMap<String, String>();
			Porperty p = f.getAnnotation(Porperty.class);
			try {
				if (p != null) {
					f.setAccessible(true);
					map.put("name", p.name());
					map.put("value", f.get(o) == null ? "" : f.get(o)
							.toString());
					map.put("group", p.group());
					map.put("editor", p.editor());
					list.add(map);
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	public static <T> List<T> toVOList(List<?> domainList, Class<T> cls) {
		List<T> list = new ArrayList<T>();
		for (Object o : domainList) {
			T t;
			try {
				t = cls.getConstructor(o.getClass()).newInstance(o);
				list.add(t);
			} catch (IllegalArgumentException e) {
			} catch (SecurityException e) {
			} catch (InstantiationException e) {
			} catch (IllegalAccessException e) {
			} catch (InvocationTargetException e) {
			} catch (NoSuchMethodException e) {
			}
		}
		return list;
	}
}
