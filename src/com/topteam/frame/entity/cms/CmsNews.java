package com.topteam.frame.entity.cms;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.topteam.utility.StringUtil;

public class CmsNews extends Model<CmsNews>{

	private static final long serialVersionUID = -8117668268468334286L;
	
	public final static CmsNews dao = new CmsNews();
	
	public Page<Record> getNewsPage(String cateid, int page, int size){
		String sql = "";
		if(StringUtil.isNull(cateid)){
			sql = "from CmsNews t order by t.order";
		}else{
			sql = "from CmsCateNews c left jion CmsNews t on c.newsid=t.id where c.cateid='"+cateid+"' order by c.order";
		}
		return Db.paginate(page, size, "select t.* ", sql);
	}
}
