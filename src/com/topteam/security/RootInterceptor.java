package com.topteam.security;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import com.jfinal.log.Logger;

/**
 * 安全验证拦截器
 * @author 姜枫  2013-5-8
 *
 */
public class RootInterceptor implements Interceptor{
	
	public static Logger logger = Logger.getLogger(RootInterceptor.class);
	

	public void intercept(ActionInvocation ai) {
		Controller controller = ai.getController();
		String actionKey = ai.getActionKey();
		String root = controller.getRequest().getContextPath();
		if(actionKey.equals("/")||actionKey.startsWith(root)){
			ai.invoke();
		}else{
			controller.redirect(root+actionKey);
		}
	}

}
