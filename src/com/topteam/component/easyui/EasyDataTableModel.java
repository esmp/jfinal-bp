package com.topteam.component.easyui;

import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Page;
import com.topteam.frame.controllor.BaseController;

public abstract class EasyDataTableModel {

	private BaseController controller;

	private boolean paginate;

	public EasyDataTableModel(BaseController controller) {
		this.paginate = true;
		this.controller = controller;
	}

	public EasyDataTableModel() {
		this.paginate = false;
	}

	public Page<?> fechData(int page, int size) {
		return null;
	}

	public Page<?> fechData() {
		return null;
	}

	public String toJson() {
		Page<?> p = null;
		if (paginate) {
			int page = controller.getParaToInt("page");
			int size = controller.getParaToInt("rows");
			p = fechData(page, size);
		} else {
			p = fechData();
		}

		Paginate paginate = Paginate.build(p);
		return JsonKit.toJson(paginate);
	}

}
