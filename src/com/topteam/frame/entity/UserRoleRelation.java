package com.topteam.frame.entity;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.ModelBean;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.topteam.utility.StringUtil;


@ModelBean("UserRoleRelation")
public class UserRoleRelation extends Model<UserRoleRelation>{

	public final static UserRoleRelation dao = new UserRoleRelation();
	
	
	public void deleteUserRoleRelationByUserId(String userId){
		String sql = "delete from UserRoleRelation where userId=?";
		Db.update(sql, userId);
	}


	public List<UserRoleRelation> getUserRoleRelationByUserId(String userId) {
		String sql = "select * from UserRoleRelation where userId=?";
		return find(sql, userId);
	}
	
	public Page<Record> getUserByDeptCode(String code,int page,int size){
		Page<Record> p = null;
		if(StringUtil.isNotNull(code)){
			p = Db.paginate(page, size, "select u.*", "from FrameUser u, UserDeptRelation t, FrameDept d where u.id=t.userid and t.deptid=d.id and d.deptcode like '"+code+"%'  order by  u.order desc");
		}else{
			p = Db.paginate(page, size, "select t.*", "from FrameUser t  order by t.order desc");
		}
		return p;
	}
	
	public List<UserRoleRelation> queryAll(){
		String sql = "select * from UserRoleRelation";
		return find(sql);
	}
	
	public boolean exsitUserRole(String userId, String roleId){
		String sql = "select count(*) from UserRoleRelation where userId=? and roleId=?";
		return Db.queryLong(sql, userId,roleId) >0L;
	}
}
