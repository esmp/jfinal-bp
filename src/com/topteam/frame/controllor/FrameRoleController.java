package com.topteam.frame.controllor;

import java.util.List;
import java.util.UUID;

import com.jfinal.plugin.activerecord.Page;
import com.topteam.component.easyui.Paginate;
import com.topteam.core.Path;
import com.topteam.frame.entity.FrameRole;
import com.topteam.frame.entity.FrameRoleType;


@Path("/framerole")
public class FrameRoleController extends BaseController {

	public void list() {
		render("/frame/FrameRoleList.html");
	}

	public void table() {
		int page = getParaToInt("page");
		int size = getParaToInt("rows");
		Page<FrameRole> list = FrameRole.dao.getAllRole(page, size);
		Paginate paginate = Paginate.build(list);
		renderJson(paginate);
	}
	
	public void getRoleType(){
		List<FrameRoleType> list = FrameRoleType.dao.getAllRoleTypeNoPaginate();
		FrameRoleType roleType = new FrameRoleType();
		roleType.set("id", "");
		roleType.set("roleTypeName", "所有角色");
		list.add(0, roleType);
		renderJson(list);
	}

	public void add() {
		FrameRole m = getModel(FrameRole.class, "FrameRole");
		List<FrameRoleType> list = FrameRoleType.dao.getAllRoleTypeNoPaginate();
		for(FrameRoleType frameRoleType:list){
		  if(m.get("roleTypeId").equals(frameRoleType.get("id"))){
			  m.set("roleTypeName",frameRoleType.get("roleTypeName"));
		  }
		}
		m.set("id", UUID.randomUUID().toString());
		m.save();
		renderText("保存成功", "text/html");
	}
	
	public void update() {
		FrameRole m = getModel(FrameRole.class, "FrameRole");
		List<FrameRoleType> list = FrameRoleType.dao.getAllRoleTypeNoPaginate();
		for(FrameRoleType frameRoleType:list){
		  if(m.get("roleTypeId").equals(frameRoleType.get("id"))){
			  m.set("roleTypeName",frameRoleType.get("roleTypeName"));
		  }
		}
		m.update();
		renderText("修改成功","text/html");
	}
	
	public void edit(){
		FrameRole d = FrameRole.dao.findById(getPara());
		setAttr("FrameRole", d);
		renderFreeMarker("/frame/FrameRoleEditPage.html");
	}
	
	public void delete(){
		String ids = getPara("ids");
		FrameRole.dao.deleteIds(ids);
		renderJson();
	}
}
