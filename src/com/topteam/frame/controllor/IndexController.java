package com.topteam.frame.controllor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.topteam.component.easyui.EasyAccordion;
import com.topteam.component.easyui.EasyTreeModel;
import com.topteam.component.easyui.EasyTreeNode;
import com.topteam.core.Path;
import com.topteam.frame.entity.FrameMenu;
import com.topteam.security.UserSession;

@Path("/")
public class IndexController extends BaseController{
	
	
	
	/**
	 * 系统首页
	 */
	public void index(){
		//TODO 从配置中获取首页的路径
		String	indexUrl = "index.html";
		
		List<FrameMenu> list = getUserSession().getRootMenu();
		//List<FrameMenu> list = FrameMenu.dao.getRootMenu();
		setAttr("acc", list);
		setAttr("cUser",getUserSession());
		render(indexUrl);
	}
	
	public void main(){
		render("/frame/main.html");
	}
	
	public void leftTree(){
		EasyTreeModel<FrameMenu> treeModel = new EasyTreeModel<FrameMenu>(this) {
			private static final long serialVersionUID = -4465033743516102585L;

			@Override
			public EasyTreeNode model2Node(FrameMenu t) {
				EasyTreeNode treeNode = new EasyTreeNode();
				treeNode.setId(t.getStr("id"));
				treeNode.setText(t.getStr("menuName"));
				treeNode.setIconCls(t.getStr("menuIcon"));
				Map<String, String> map = new HashMap<String, String>();
				map.put("url", t.getStr("menuUrl"));
				treeNode.setAttributes(map);
				treeNode.setHasChild(getUserSession().getMenusById(t.getStr("id")).size()>0);
				return treeNode;
			}
			
			@Override
			public List<FrameMenu> fechDate(String id) {
				System.out.println(id);
				List<FrameMenu> list = new ArrayList<FrameMenu>();
				if(id == null){
					id = getRootId();
				}else{
//					String sql = "select t.*, case when (SELECT count(1) from FrameMenu t2 where t2.parentId=t.id)>0 then 1 ELSE 0 END as has  from FrameMenu t where t.parentId=? order by t.order desc";
//					list = FrameMenu.dao.find(sql,id);
					list = getUserSession().getMenusById(id);
				}
				return list;
			}
		};
		
		renderJson(treeModel.toJson());
	}
	
	public void changeTheme(){
		String theme = getPara();
		UserSession session = getUserSession();
		session.setUserTheme("metro-"+theme);
		
		//TODO 保存-修改后的用户主题
		
		redirect("/index");
	}
	
	public void popMsg(){
		UserSession userSession = getUserSession();
		//TODO 查询当前用户拥有的消息提示
		
		String msg = "您有<font color='red'>2</font>封新邮件";
		renderText(msg);
	}
	
}
