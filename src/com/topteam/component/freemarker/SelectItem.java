package com.topteam.component.freemarker;

public class SelectItem {

	private String text;

	private String value;

	private boolean selected;

	public SelectItem() {
		super();
	}

	public SelectItem(String text, String value) {
		super();
		this.text = text;
		this.value = value;
	}

	public SelectItem(String text, String value, boolean selected) {
		super();
		this.text = text;
		this.value = value;
		this.selected = selected;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
