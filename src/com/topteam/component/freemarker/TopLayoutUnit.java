package com.topteam.component.freemarker;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.topteam.frame.controllor.BaseController;
import com.topteam.utility.StringUtil;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class TopLayoutUnit extends TopTemplateModel {
	public final static String TAG = "layoutunit";

	private BaseController controller;

	private String id;

	private String style;

	private String position;

	private boolean split;

	private String title;

	private int height;

	private int width;

	private String icon;

	private boolean collapsible = true;

	private boolean border = true;
	
	private String href;

	public TopLayoutUnit(BaseController controller) {
		this.controller = controller;
	}

	private void initPara(Map params, Environment env) throws TemplateException {
		id = getStrPara("id", params, env);
		style = getStrPara("style", params, env);
		position = getStrPara("position", params, env);
		if (StringUtil.isNull(position)) {
			throw new TemplateException("LayoutUnit 控件必须设置 position 的值", env);
		}
		title = getStrPara("title", params, env);
		icon = getStrPara("icon", params, env);
		split = getBoolPara("split", params, env, split);
		collapsible = getBoolPara("collapsible", params, env, collapsible);
		border = getBoolPara("border", params, env, border);
		height = getIntPara("height", params, env);
		width = getIntPara("width", params, env);
		href = getStrPara("href", params, env);
	}

	@Override
	public void execute(Environment env, Map para, TemplateModel[] arg2,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		initPara(para, env);
		Writer w = env.getOut();
		w.write("<div class=\"easyui-layout\" style=\""+style+"\" data-options=\"");
		if(StringUtil.isNotNull(title))
			w.write("title:'"+title+"',");
		if(StringUtil.isNotNull(icon))
			w.write("iconCls:'"+icon+"',");
		if(StringUtil.isNotNull(href))
			w.write("href:'"+href+"',");
		
		if(!position.equals("center")){
			w.write("split:"+split+",");
			w.write("collapsible:"+collapsible+",");
			w.write("border:"+border+",");
		}
		w.write("region:'"+position+"'");
		w.write("\" >");
		try{
			body.render(w);
		}catch(Exception e){
			
		}
		w.write("</div>");
	}
}
